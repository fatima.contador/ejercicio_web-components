      class MiContenedor extends HTMLElement{
              constructor(){
               super();

              }
      }
      customElements.define('mi-contenedor',MiContenedor);
      
      const host = document.querySelector('#host');
      const tpl = document.querySelector('template');
      const tplInst = tpl.content.cloneNode(true);
      host.appendChild(tplInst);


        function enConsola (text){
        console.log(text);
        }
        
        function enPantalla(datos){

        var DatosJson = JSON.parse(JSON.stringify(datos));               

        for (let i = 0; i < DatosJson.data.length; i++){

         let renglon = document.createElement("tr");

        /*var names = [id,employee_name,employee_salary,employee_age,profile_image];
        for (var value of names) {
           let columna = document.createElement("td");
           let texto = document.createTextNode(DatosJson.data[i].value);
           columna.appendChild(texto);
           renglon.appendChild(columna);
         //}*/

         for (let j = 0; j < 6; j++) {
           let columna = document.createElement("td");
           let texto = null;
           let fullName = DatosJson.data[i].employee_name;
           let spl = fullName.split(" ");
                if (j == 0){
                texto = document.createTextNode(DatosJson.data[i].id);
                }else if(j == 1){
                texto = document.createTextNode(spl[0]);
                }else if(j == 2){
                texto = document.createTextNode(spl[1]);
                }else if(j == 3){
                texto = document.createTextNode(DatosJson.data[i].employee_salary);
                }else if(j == 4){
                texto = document.createTextNode(DatosJson.data[i].employee_age);
                }else{
                texto = document.createTextNode(DatosJson.data[i].profile_image);  
                }
           columna.appendChild(texto);
           renglon.appendChild(columna);
         }

        const x = document.querySelector('#tableBody').appendChild(renglon);

        }

        }
            
const req = new XMLHttpRequest();
req.addEventListener('load', () =>{
    if(req.status === 200){
        enConsola( req.response);
        
        enPantalla(req.response);
    }else{
        enConsola([req.status, req.statusText]);
    }
});

req.addEventListener('error',() => {
    enConsola('Error de red');
});

req.open('GET','js/datos.json',true);
//req.open('GET','https://dummy.restapiexample.com/api/v1/employees',true);
req.responseType = 'json';
req.send(null);
      

document.querySelector('#busca').onkeyup = function(){
        $resultados('#tabla',this.value);
}

$resultados = function (id,value){
        var rows = document.querySelectorAll(id + ' tbody tr');
        for (var i = 0; i < rows.length; i++) {
                
               var showRow = false;
               var row = rows[i];
                row.style.display = 'none';

                for (var j = 0; j < row.childElementCount; j++) {
                    if (row.children[j].textContent.toLowerCase().indexOf(value.toLowerCase().trim()) > -1){
                            showRow = true;
                            break;
                    }
                        
                }
                if (showRow){
                row.style.display = null;
                }
        }
}